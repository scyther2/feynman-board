import React from "react";
import { Popover } from "react-bootstrap";

const CategoryPopover: React.FC = () => {
  return (
    <Popover id={`popover-positioned-top`}>
      <Popover.Header as="h3">{`Popover top`}</Popover.Header>
      <Popover.Body>
        <strong>Holy guacamole!</strong> Check this info.
      </Popover.Body>
    </Popover>
  );
};

export default CategoryPopover;
