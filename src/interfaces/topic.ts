export interface Topic {
  name: string;
  _id: string;
  __v: number;
  percentage: number
}
export enum Category { "UNDERSTOOD", "SOMEWHAT UNDERSTOOD", "NOT CLEAR", "WHAT RUBBISH" }

export const categories = ["UNDERSTOOD", "SOMEWHAT UNDERSTOOD", "NOT CLEAR", "WHAT RUBBISH"]
export interface Block {
  content: string;
  category: string;
}
