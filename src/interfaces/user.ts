export interface UserObject {
    user: User;
}

export interface User {
    userName: string;
    topics:   any[];
    _id:      string;
    __v:      number;
}