import { Block } from "../interfaces/topic";

export const getUser = async (userName: string | undefined) => {
    return fetch(`${process.env.REACT_APP_BACKEND_API}/user/${userName}`, { method: "GET" })
        .then((res) => res.json())
        .catch((err) => console.log(err));
}

export const createTopic = async (topicName: string) => {
    let options = {
        method: 'POST',
        headers: {
            'User-Agent': 'React Front-End',
            Accept: 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({ name: topicName })
    };

    return fetch(`${process.env.REACT_APP_BACKEND_API}/topic/create`, options)
        .then((res) => res.json())
        .catch((err) => console.log(err));
}

export const updateTopic = async (topicID: string, blocks: Block[]) => {
    let options = {
        method: 'POST',
        headers: {
            'User-Agent': 'React Front-End',
            Accept: 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({ topicID: topicID, blocks: blocks, percentage: calculatePercentage(blocks) })
    };
    return fetch(`${process.env.REACT_APP_BACKEND_API}/topic/updateContent`, options)
        .then((res) => res.json())
        .catch((err) => console.log(err));
}

export const calculatePercentage = (blocks: Block[]) => {
    let scoreArray = blocks.map((block) => (getScore(block)))
    let sum = scoreArray.reduce((a, b) => a + b, 0)
    let Percentage = (sum / (scoreArray.length * 4)) * 100
    return Math.floor(Percentage)
}

export const getScore = (block: Block) => {
    switch (block.category) {
        case "WHAT RUBBISH":
            return 1
        case "NOT CLEAR":
            return 2
        case "SOMEWHAT UNDERSTOOD":
            return 3
        case "UNDERSTOOD":
            return 4

        default:
            return 1
    }
}