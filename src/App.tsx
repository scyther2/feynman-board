import React from "react";
import "./App.css";
import AppRoutes from "./AppRoutes";

const App: React.FC = () => {
  return (
    <div className="App">
      <header className="App-header">
      <AppRoutes />
      </header>
    </div>
  );
};

export default App;
