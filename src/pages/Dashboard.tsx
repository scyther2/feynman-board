import React, { useState, useEffect } from "react";
import { useParams, useNavigate } from "react-router-dom";
import { InputGroup, Form, Button, ListGroup, Badge } from "react-bootstrap";
import { Topic } from "../interfaces/topic";
import { createTopic, getUser } from "../api/api";
import { UserObject } from "../interfaces/user";

const Dashboard: React.FC = () => {
  const [userdata, setUserdata] = useState<UserObject | null>(null);
  const [topicName, setTopicName] = useState<string>("");
  const [topicData, setTopicData] = useState<Topic | null>(null);
  const { userName } = useParams();
  const navigate = useNavigate();

  const handleCreateTopic = async () => {
    try {
      createTopic(topicName).then((createdTopicData) => {
        setTopicData(createdTopicData);
        navigate(`${topicData?.name}`, {
          state: { topicData: createdTopicData },
        });
      });
    } catch (error) {
      setTopicData(null);
    }
  };

  useEffect(() => {
    getUser(userName).then((data) => {
      setUserdata(data);
    });
  }, [userName]);
  console.log(userdata);
  return (
    <div>
      <h1>{`Welcome ${userName}`}</h1>
      <InputGroup className="mb-3" style={{ width: "400px" }}>
        <Form.Control
          placeholder="Topic Name"
          aria-label="Topic Name"
          aria-describedby="basic-addon1"
          value={topicName}
          onChange={(e) => {
            setTopicName(e.target.value);
          }}
        />
        <Button
          variant="dark"
          onClick={() => {
            handleCreateTopic();
          }}
        >
          Create Topic
        </Button>
      </InputGroup>
      {userdata?.user?.topics && (
        <ListGroup as="ol" numbered>
          {userdata?.user?.topics.map((topic: Topic) => (
            <ListGroup.Item
              key={topic._id}
              as="li"
              className="d-flex justify-content-between align-items-start"
            >
              <div className="ms-2 me-auto">
                <div className="fw-bold">{topic.name}</div>
              </div>
              <Badge bg="primary" pill>
                {topic?.percentage} %
              </Badge>
            </ListGroup.Item>
          ))}
        </ListGroup>
      )}
    </div>
  );
};

export default Dashboard;
