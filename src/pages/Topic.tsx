import React, { useEffect, useState } from "react";
import { useParams, useNavigate, useLocation } from "react-router-dom";
import {
  InputGroup,
  Form,
  Button,
  OverlayTrigger,
  Popover,
} from "react-bootstrap";
import { Block, categories } from "../interfaces/topic";
import { updateTopic } from "../api/api";

const Topic: React.FC = () => {
  let { topicName, userName } = useParams();
  const navigate = useNavigate();
  const { state } = useLocation();
  console.log(state);
  // const [name, setName] = useState<string | undefined>(topicName);
  const [content, setContent] = useState<string>("");
  const [blocks, setBlocks] = useState<Block[]>([]);
  useEffect(() => {
    let tempBlocks = content
      .trim()
      .split(/([\n,.{}[\]()'"\\/;:?|-])+/)
      .filter((element) => Boolean(element));

    let newTempBlocks = [];
    for (let i = 0; i < tempBlocks.length; i += 2) {
      newTempBlocks.push(tempBlocks[i] + (tempBlocks[i + 1] || ""));
    }
    let parsedBlocks = newTempBlocks.map((block) => ({
      content: block,
      category: "NOT CLEAR",
    }));
    setBlocks(parsedBlocks);
  }, [content]);

  const getBlockColor = (block: Block) => {
    switch (block.category) {
      case "NOT CLEAR":
        return "#00AFEF";
      case "UNDERSTOOD":
        return "#A8D08D";
      case "SOMEWHAT UNDERSTOOD":
        return "#FFD866";
      case "WHAT RUBBISH":
        return "#FF0000";

      default:
        return "#BEBEBE";
    }
  };
  const getBlockColorCat = (category: string) => {
    switch (category) {
      case "NOT CLEAR":
        return "#00AFEF";
      case "UNDERSTOOD":
        return "#A8D08D";
      case "SOMEWHAT UNDERSTOOD":
        return "#FFD866";
      case "WHAT RUBBISH":
        return "#FF0000";

      default:
        return "#BEBEBE";
    }
  };

  return (
    <div>
      <InputGroup className="mb-3" style={{ width: "400px" }}>
        {/* <Form.Control
          placeholder="Username"
          aria-label="Username"
          aria-describedby="basic-addon1"
          value={name}
          onChange={(e) => {
            setName(e.target.value);
          }}
          disabled
        /> */}
        {state.topicData.name}
      </InputGroup>
      <Form.Control
        as="textarea"
        rows={15}
        placeholder="Content"
        aria-label="Content"
        aria-describedby="basic-addon1"
        value={content}
        onChange={(e) => {
          setContent(e.target.value);
        }}
      />
      <InputGroup
        className="m-3"
        style={{
          width: "400px",
          padding: 10,
          backgroundColor: "#e9ecef",
          borderRadius: "10px",
        }}
      >
        {blocks.map((block, index) => (
          <OverlayTrigger
            key={index}
            trigger="click"
            placement="top"
            rootCloseEvent="click"
            overlay={
              <Popover id={`popover-positioned-top`}>
                <Popover.Header as="h3">Select Category</Popover.Header>
                <Popover.Body>
                  <Form>
                    <div className="mb-3">
                      {categories.map((cat) => (
                        <div key={cat}>
                          <input
                            type="radio"
                            className="btn-check"
                            name="options"
                            id={cat}
                            onChange={(e) => {
                              let tempBlock = {
                                ...block,
                                category: e.target.id,
                              };
                              let tempBlocks = [...blocks];
                              tempBlocks[index] = tempBlock;
                              setBlocks(tempBlocks);
                            }}
                          />
                          <label
                            className="btn"
                            htmlFor={cat}
                            style={{
                              backgroundColor: getBlockColorCat(cat),
                              marginBottom: "4px",
                            }}
                          >
                            <strong>{cat}</strong>
                          </label>
                        </div>
                      ))}
                    </div>
                  </Form>
                </Popover.Body>
              </Popover>
            }
          >
            <InputGroup.Text
              style={{
                border: "none",
                color: getBlockColor(block),
                cursor: "pointer",
              }}
            >
              <strong>{block.content}</strong>
            </InputGroup.Text>
          </OverlayTrigger>
        ))}
      </InputGroup>
      <Button
        className="mt-2"
        variant="success"
        onClick={() => {
          updateTopic(state?.topicData._id, blocks).then((data) => {
            console.log(data);
            return navigate(`/user/${userName}`);
          });
        }}
      >
        Save Content
      </Button>
    </div>
  );
};

export default Topic;
