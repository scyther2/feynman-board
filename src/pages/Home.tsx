import React, { useState } from "react";
import { InputGroup, Form, Button } from "react-bootstrap";
import { useNavigate } from "react-router-dom";
import { getUser } from "../api/api";

const Home: React.FC = () => {
  const [userName, setUserName] = useState<string>("");
  const navigate = useNavigate();
  const handleSearch = () => {
    getUser(userName).then((userData) => {
      console.log(userData);
      navigate(`user/${userData?.user.userName}`, {
        state: { data: userData },
      });
    });
  };

  return (
    <div>
      {/* {data && <Navigate to={`user/${data?.user.userName}`} />} */}
      <h1>Feynman Board</h1>
      <p>Enter your Username to continue: </p>
      <InputGroup className="mb-3" style={{ width: "400px" }}>
        <Form.Control
          placeholder="Username"
          aria-label="Username"
          aria-describedby="basic-addon1"
          value={userName}
          onChange={(e) => {
            setUserName(e.target.value);
          }}
        />
        <Button
          variant="dark"
          onClick={() => {
            handleSearch();
          }}
        >
          Search
        </Button>
      </InputGroup>
    </div>
  );
};

export default Home;
