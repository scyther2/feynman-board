import { BrowserRouter, Routes, Route } from "react-router-dom";
import Home from "./pages/Home";
import Dashboard from "./pages/Dashboard";
import Topic from "./pages/Topic";

const AppRoutes = () => {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="user/:userName" element={<Dashboard />} />
        <Route path="user/:userName/:topicName" element={<Topic />} />
      </Routes>
    </BrowserRouter>
  );
};

export default AppRoutes;
